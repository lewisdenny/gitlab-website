---
title: "About Me"
date: 2022-12-20T11:04:49+10:00
draft: false
---
#### Summary
I have worked as a Software Maintenance Engineer and a Technical Support Engineer at Red Hat, and as a Datacenter 
Engineer and Hardware Engineer at Interactive.  
  
I have made upstream contributions to OpenStack and have written several personal projects, including an ESP32 
server exhaust fan controller in C and various ansible playbooks. I have also been involved in internal work
projects at Red Hat, including Phoenix, a database server on demand, and Gobot, a multiplatform chat bot.  
I have also obtained certifications in RHCSA (Red Hat Certified System Administrator), RHCE (Red Hat Certified
Engineer), and C Programming with Linux. 
  
Overall, I have a diverse set of skills and experience in the field of software engineering and technical support.
I have experience working with various programming languages and technologies, and have a strong understanding of
Linux systems. I also have experience leading and managing teams, as demonstrated by my role as a restaurant manager.
I would like to continue improving my developer skills and use my experience in the cloud space to become a Software Engineer.  

#### Strengths and Expertise
- SDN Networking
  - Neutron
  - OVN
- OpenStack
- OpenShift
- Linux Systems
- Programming
- Leadership and Mentorship


#### Employment History
- **:(fab fa-redhat): Red Hat (2019 - Present)**
  - Software Maintenance Engineer
  - Technical Support Engineer
  - Associate Technical Support Engineer
- **:(fas fa-rotate): Interactive (2016 - 2019)**
  - Datacenter Engineer
  - Hardware Engineer
- **:(fas fa-drumstick-bite): KFC (2014 - 2016)**
  - Restaurant Manager


#### Upstream Contributions
- OpenStack - [Gerrit](https://review.opendev.org/q/owner:ldenny%2540redhat.com)
- OpenStack  - [GitHub](https://github.com/search?q=org%3Aopenstack-k8s-operators++author%3Alewisdenny&type=pullrequests)

####  Personal Projects:
- [ESP32 Server Exhaust Fan Controller](https://gitlab.com/lewisdenny/esp32-server-exhaust-fan)
  - Written in C, I created this project after finishing my C programming course. It incorporates C, 
microcontrollers and [Home Assistant](https://www.home-assistant.io/), all things I’m personally interested in. 
- [Various Anisble Playbooks I've written](https://gitlab.com/lewisdenny/ansible)

#### Internal Work Projects (Red Hat VPN required):
- [**Phoenix - Database server on demand**](https://gitlab.cee.redhat.com/tme/phoenix-dbsass): This is a full stack project I wrote to help support engineers inspect and manipulate database backups. The engineer provides a case number and attachment name using the frontend web interface and the backend downloads the attachment, spins up a podman container and provides the access details to the support engineer.
    - **:(fas fa-forward fa-fw):Frontend:** Written in JS using the [Vue3 framework](https://vuejs.org/) connecting to the backend using the [AXIOS library](https://github.com/axios/axios).
    - **:(fas fa-backward fa-fw):Backend:** Written in Python using the [FastAPI framework](https://fastapi.tiangolo.com/), this handles authentication with the ticking system and interfacing with podman using the [podman-py library](https://github.com/containers/podman-py)
- **Gobot - A Multiplatform Chat Bot:** I am one of the core maintainers for the Gobot project, a collection of software that is used by thousands of Red Hatters each day to monitor there case work, give karma to each other and provide access to many useful services right form the chat platform.   
  - [IRC Bot](https://gitlab.cee.redhat.com/tme/gobot): Using the [go-irc](https://github.com/go-irc/irc) library
  - [Slack Bot](https://gitlab.cee.redhat.com/tme/gobot-slack): Using the [slack-go](https://github.com/slack-go/slack) library
  - [API Server](https://gitlab.cee.redhat.com/tme/gobot-api-next): Using the [go-restful](https://github.com/emicklei/go-restful) library

#### Certifications
- [RHCSA :(fab fa-redhat fa-fw):](https://www.credly.com/badges/cc6176da-843f-4e86-b025-8d6c00b4f145/public_url)
- [RHCE :(fab fa-redhat fa-fw):](https://www.credly.com/badges/9df694b1-626a-481c-b059-c97b843619e3/public_url)
- [C Programming with Linux](https://credentials.edx.org/credentials/a27ef76444c9417f8004b09912ed6bf7/)

#### Links:
- https://gitlab.cee.redhat.com/ldenny
- https://gitlab.com/lewisdenny
- https://lewisdenny.io
- https://github.com/lewisdenny