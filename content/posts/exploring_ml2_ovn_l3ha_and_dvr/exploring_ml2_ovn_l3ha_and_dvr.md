---
title: "Exploring ml2_ovn L3HA and DVR"
date: 2022-03-11T10:53:04+10:00
draft: true
description: "This article explores the L3HA and DVR features of OVN"
tags: ["OpenStack", "Networking", "ONV"]
categories: ["OpenStack"]
code:
  maxShownLines: 200
toc:
  auto: false
  enable: true
  keepStatic: false
---

This article explores the L3HA and DVR features of OVN.

<!--more-->

{{< admonition tip >}}
This document is WIP
{{< /admonition >}}

## Overview

When using `ml2_ovn` with OpenStack Neutron we have some confusing terms to understand, especially confusing if your used to using the `ml2_ovs` plugin. In this article I hope to demystify two terms, `L3HA`(Layer 3 High Availability) and `DVR`(Distributed Virtual Routing)

The environment I'm using is Red Hat OpenStack Platform 16.2(train) deployed with (Infrared)[https://github.com/redhat-openstack/infrared], this means we will have DVR enabled by default. 

## L3HA
L3HA mode is built in to OVN, it uses BFD monitoring along with OVN chassis' marked with the CMS option `enable-chassis-as-gw` to enable the ability to have highly available FIP(Floating IP) traffic. With RHOSP16.2 we set the `enable-chassis-as-gw` CMS option on the Controller node chassis' by default in both DVR[1] and non dvr deployments[2]. In L3HA mode if the chassis servicing the FIP traffic is brought down then the next chassis in the BFD priority will take over automatically.

## DVR
DVR mode is also built into OVN, as you can see above both L3HA and DVR can and are enabled at the same time. DVR mode instructs OVN to set the `external_mac` value on the FIP NAT record:
```
[root@compute-0 heat-admin]# ovn-nbctl list nat
_uuid               : e480d927-3a69-4805-aae6-08f2025be79b
allowed_ext_ips     : []
exempted_ext_ips    : []
external_ids        : {"neutron:fip_external_mac"="fa:16:3e:83:39:ee", "neutron:fip_id"="a0b8ab06-0ba3-446b-adc9-1aec9acfb155", "neutron:fip_network_id"="bd7e2d46-a9ec-4248-a554-217a50f20834", "neutron:fip_port_id"="9baf46bb-1503-41df-9846-5958cec49d25", "neutron:revision_number"="2", "neutron:router_name"=neutron-8bf667ea-ee50-4c1d-b0f0-18bcc0d0e9fe}
external_ip         : "10.0.0.203"
external_mac        : "fa:16:3e:83:39:ee"
external_port_range : ""
logical_ip          : "172.20.1.68"
logical_port        : "9baf46bb-1503-41df-9846-5958cec49d25"
options             : {}
type                : dnat_and_snat
```

When the `external_mac` value is set OVN knows this compute node should handle the FIP traffic for this NAT entry, if I ping this FIP and check all the chassis' I can see the traffic coming in on the external interface of `compute-0`:
```shell
$ ansible -i inventory.yaml overcloud -m shell -a 'tcpdump -nnei ens5 -c 3 2> /dev/null | grep ICMP' -b

compute-0 | CHANGED | rc=0 >>
09:10:51.700950 52:54:00:bc:88:5d > fa:16:3e:83:39:ee, ethertype IPv4 (0x0800), length 98: 10.0.0.65 > 10.0.0.203: ICMP echo request, id 49808, seq 1218, length 64
09:10:51.701194 fa:16:3e:83:39:ee > 52:54:00:bc:88:5d, ethertype IPv4 (0x0800), length 98: 10.0.0.203 > 10.0.0.65: ICMP echo reply, id 49808, seq 1218, length 64
controller-0 | FAILED | rc=1 >>
non-zero return code
controller-1 | FAILED | rc=1 >>
non-zero return code
compute-1 | FAILED | rc=1 >>
non-zero return code
controller-2 | FAILED | rc=1 >>
non-zero return code
```

{{< admonition note >}}
This command is taking a sample of 3 packets from ens5 on each of the overcloud nodes and checking them for ICMP packets, please change to match your environment. Inventory can be generated with instructions from (here)[https://lewisdenny.io/ovs_ovn_command_cheat_sheet/#check-all-versions-are-matching]
{{< /admonition >}}


However with DVR if I were to take down the external interface of the compute node the ping fails:

```
$ ovs-vsctl show
[...]
    Bridge br-ex
        fail_mode: standalone
        Port br-ex
            Interface br-ex
                type: internal
        Port ens5
            Interface ens5
[...]

$ ovs-vsctl del-port br-ex ens5

$ ping -O -c 3 10.0.0.203

PING 10.0.0.203 (10.0.0.203) 56(84) bytes of data.
no answer yet for icmp_seq=1
no answer yet for icmp_seq=2
From 10.0.0.65 icmp_seq=1 Destination Host Unreachable
From 10.0.0.65 icmp_seq=2 Destination Host Unreachable
From 10.0.0.65 icmp_seq=3 Destination Host Unreachable
```
{{< admonition note >}}
If we were to bring down the openvswitch service or tunnel interface we would have no way to route the packet to the compute node anyway.
{{< /admonition >}}


## Force DVR NAT rule to L3HA mode

As I said L3HA mode is also enabled by fault and built into OVN however the `external_mac` being set overrides this. We can clear this `external_mac` record and explore what will happen in L3HA mode:
```
$ ovn-nbctl clear nat e480d927-3a69-4805-aae6-08f2025be79b external_mac

$ ovn-nbctl list nat

_uuid               : e480d927-3a69-4805-aae6-08f2025be79b
allowed_ext_ips     : []
exempted_ext_ips    : []
external_ids        : {"neutron:fip_external_mac"="fa:16:3e:83:39:ee", "neutron:fip_id"="a0b8ab06-0ba3-446b-adc9-1aec9acfb155", "neutron:fip_network_id"="bd7e2d46-a9ec-4248-a554-217a50f20834", "neutron:fip_port_id"="9baf46bb-1503-41df-9846-5958cec49d25", "neutron:revision_number"="2", "neutron:router_name"=neutron-8bf667ea-ee50-4c1d-b0f0-18bcc0d0e9fe}
external_ip         : "10.0.0.203"
external_mac        : []
external_port_range : ""
logical_ip          : "172.20.1.68"
logical_port        : "9baf46bb-1503-41df-9846-5958cec49d25"
options             : {}
type                : dnat_and_snat
```

Once the `external_mac` record has been cleared the traffic will immediately start routing through the controller node with the highest BFD priority. We can check this with the following commands:

List all gateway chassis', we can see that `67227cc4` has the priority:
```
$ ovn-nbctl list gateway_chassis

_uuid               : 67227cc4-a7f1-42a5-8d28-71b75a082aab
chassis_name        : "430c682f-38ab-4e7f-9dc7-dfc46a327727"
external_ids        : {}
name                : lrp-6f0d474f-ce98-4065-9ede-a76d724b2151_430c682f-38ab-4e7f-9dc7-dfc46a327727
options             : {}
priority            : 3

_uuid               : b3f38645-a918-427c-8c39-3f84cb65564d
chassis_name        : "6f968e7c-96f1-4255-b966-5c637bb02311"
external_ids        : {}
name                : lrp-6f0d474f-ce98-4065-9ede-a76d724b2151_6f968e7c-96f1-4255-b966-5c637bb02311
options             : {}
priority            : 2

_uuid               : cce27e13-0bea-44a4-ae7e-30538947f245
chassis_name        : "dfc1c122-7e14-44e1-a723-ed4a4a9f3959"
external_ids        : {}
name                : lrp-6f0d474f-ce98-4065-9ede-a76d724b2151_dfc1c122-7e14-44e1-a723-ed4a4a9f3959
options             : {}
priority            : 1
```

Find out hostname of `67227cc4` by using the `chassis_name` value:
```
[root@compute-0 heat-admin]# ovn-sbctl  list  chassis 430c682f-38ab-4e7f-9dc7-dfc46a327727

_uuid               : caa26c40-7027-4587-adac-13ecaeabdbcc
encaps              : [04c36145-e6ed-4b47-be6a-d73ae74eed05]
external_ids        : {datapath-type=system, iface-types="bareudp,erspan,geneve,gre,gtpu,internal,ip6erspan,ip6gre,lisp,patch,stt,system,tap,vxlan", is-interconn="false", ovn-bridge-mappings="datacentre:br-ex,tenant:br-isolated", ovn-chassis-mac-mappings="", ovn-cms-options=enable-chassis-as-gw, ovn-enable-lflow-cache="true", ovn-limit-lflow-cache="", ovn-memlimit-lflow-cache-kb="", ovn-monitor-all="true", ovn-trim-limit-lflow-cache="", ovn-trim-timeout-ms="", ovn-trim-wmark-perc-lflow-cache="", port-up-notif="true"}
hostname            : controller-1.redhat.local
name                : "430c682f-38ab-4e7f-9dc7-dfc46a327727"
nb_cfg              : 0
other_config        : {datapath-type=system, iface-types="bareudp,erspan,geneve,gre,gtpu,internal,ip6erspan,ip6gre,lisp,patch,stt,system,tap,vxlan", is-interconn="false", ovn-bridge-mappings="datacentre:br-ex,tenant:br-isolated", ovn-chassis-mac-mappings="", ovn-cms-options=enable-chassis-as-gw, ovn-enable-lflow-cache="true", ovn-limit-lflow-cache="", ovn-memlimit-lflow-cache-kb="", ovn-monitor-all="true", ovn-trim-limit-lflow-cache="", ovn-trim-timeout-ms="", ovn-trim-wmark-perc-lflow-cache="", port-up-notif="true"}
transport_zones     : []
vtep_logical_switches: []
```

Now lets confirm the `ICMP` packets are now hitting `controller-1` external interface and the FIP is now in L3HA mode:
```
$ ansible -i inventory.yaml overcloud -m shell -a 'tcpdump -nnei ens5 -c 3 2> /dev/null | grep ICMP' -b

controller-1 | CHANGED | rc=0 >>
20:26:43.510388 52:54:00:bc:88:5d > fa:16:3e:cf:a5:93, ethertype IPv4 (0x0800), length 98: 10.0.0.65 > 10.0.0.203: ICMP echo request, id 29231, seq 7, length 64
20:26:43.511219 fa:16:3e:cf:a5:93 > 52:54:00:bc:88:5d, ethertype IPv4 (0x0800), length 98: 10.0.0.203 > 10.0.0.65: ICMP echo reply, id 29231, seq 7, length 64
compute-1 | FAILED | rc=1 >>
non-zero return code
controller-2 | FAILED | rc=1 >>
non-zero return code
compute-0 | FAILED | rc=1 >>
non-zero return code
controller-0 | FAILED | rc=1 >>
non-zero return code
```

We can now test the High Availability feature by doing the same test as before on the compute node and removing the external interface from the OVS bridge:
```
$ ovs-vsctl show
    Bridge br-ex
        fail_mode: standalone
        Port br-ex
            Interface br-ex
                type: internal
        Port patch-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c-to-br-int
            Interface patch-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c-to-br-int
                type: patch
                options: {peer=patch-br-int-to-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c}
        Port ens5
            Interface ens5
    ovs_version: "2.15.4"

$ ovs-vsctl del-port br-ex ens5

$ ovs-vsctl show

    Bridge br-ex
        fail_mode: standalone
        Port br-ex
            Interface br-ex
                type: internal
        Port patch-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c-to-br-int
            Interface patch-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c-to-br-int
                type: patch
                options: {peer=patch-br-int-to-provnet-86e46bd7-7aeb-4ddf-8e5e-7c479251ed0c}
    ovs_version: "2.15.4"
```

```
$ ping -O -c 3 10.0.0.203

PING 10.0.0.203 (10.0.0.203) 56(84) bytes of data.
no answer yet for icmp_seq=1
no answer yet for icmp_seq=2
From 10.0.0.65 icmp_seq=1 Destination Host Unreachable
From 10.0.0.65 icmp_seq=2 Destination Host Unreachable
From 10.0.0.65 icmp_seq=3 Destination Host Unreachable
```

**NOTE** 
Bringing down the external interface dose not trigger BFD fail mode, only the tunnel interface going down and openvswitch failing.

Lets try with bringing down the `openvswitch` service and checking if the ping still works:
```
$ systemctl stop openvswitch

$ ping -O -c 3 10.0.0.203 
PING 10.0.0.203 (10.0.0.203) 56(84) bytes of data.
64 bytes from 10.0.0.203: icmp_seq=1 ttl=63 time=2.27 ms
64 bytes from 10.0.0.203: icmp_seq=2 ttl=63 time=1.24 ms
64 bytes from 10.0.0.203: icmp_seq=3 ttl=63 time=0.755 ms
```

So what must have happened is the FIP has failed over to another controller node, we know from the output above that the next node in line should be blah or `controller-2`. Let's confirm with ping:
```
$ ansible -i inventory.yaml overcloud -m shell -a 'tcpdump -nnei ens5 -c 3 2> /dev/null | grep ICMP' -b
controller-2 | CHANGED | rc=0 >>
20:56:00.199029 52:54:00:bc:88:5d > fa:16:3e:cf:a5:93, ethertype IPv4 (0x0800), length 98: 10.0.0.65 > 10.0.0.203: ICMP echo request, id 20528, seq 24, length 64
20:56:00.199554 fa:16:3e:cf:a5:93 > 52:54:00:bc:88:5d, ethertype IPv4 (0x0800), length 98: 10.0.0.203 > 10.0.0.65: ICMP echo reply, id 20528, seq 24, length 64
compute-1 | FAILED | rc=1 >>
non-zero return code
controller-0 | FAILED | rc=1 >>
non-zero return code
compute-0 | FAILED | rc=1 >>
non-zero return code
controller-1 | FAILED | rc=1 >>
non-zero return code
```











[1] https://github.com/openstack/tripleo-heat-templates/blob/stable/train/environments/services/neutron-ovn-dvr-ha.yaml#L30-L31
[2] https://github.com/openstack/tripleo-heat-templates/blob/stable/train/environments/services/neutron-ovn-ha.yaml#L30-L31






