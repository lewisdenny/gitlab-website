---
title: "Deploying standalone OpenStack using dev-install"
date: 2022-12-11T09:48:49+10:00
draft: false
description: "This article documents how to deploy a standalone OpenStack 16.2 environment great for development"
tags: ["OpenStack", "dev-install", "Guide", "Standalone"]
categories: ["OpenStack"]
toc:
  auto: false
  enable: true  
  keepStatic: false
code:
  maxShownLines: 80
---

This article documents how to deploy a standalone OpenStack 16.2 environment great for development

<!--more-->

## Why would you want to type of deployment?
There are many ways to deploy OpenStack and a few tools to do so like Infrared and Tripleo Quickstart. If you need to quickly change environment versions you don't want to be deploying them manually.  

While a traditional OpenStack environment using all baremetal servers would be best for reproducing issues, it also takes longer and uses more server resource. Tools like Infrared and Tripleo Quickstart are great, requiring only one baremetal server and giving you 
a traditional topology of director, controllers and computes for troubleshooting clustering issues for example.  
However, for running OpenShift on top of OpenStack, dev-install's standalone deployment method is best, this allows you to save the maximum amount of resources for the 
OpenShift environment while configuring all the OpenStack services on the baremetal server and not adding any additional virtualization layer.

---

## Prerequisites
- Bastion server  
- Single high capacity server to be used as the `standalone host`

---

## Configuring the standalone host
In the guide we will be installing RHOSP16.2, unlike other methods, dev-install installs a standalone version of OpenStack directly on the server. This means unlike other development deployment methods that utilise virtualization, dev-install requires the correct underlying OS to be installed which would be `RHEL8.4`.


Copy ssh key from bastion node:
```bash
ssh-copy-id root@standalone_host.example.com
```


Access the `standalone_host`
```bash
ssh 'root@standalone_host.example.com'
```


Verify the storage configuration:
```bash
[root@standalone_host ~]# df -h
Filesystem                                  Size  Used Avail Use% Mounted on
devtmpfs                                    189G     0  189G   0% /dev
tmpfs                                       189G     0  189G   0% /dev/shm
tmpfs                                       189G  9.7M  189G   1% /run
tmpfs                                       189G     0  189G   0% /sys/fs/cgroup
/dev/mapper/rhel_standalone_host-root   70G  2.3G   68G   4% /
/dev/mapper/rhel_standalone_host-home  1.7T   12G  1.7T   1% /home
/dev/sda1                                  1014M  185M  830M  19% /boot
tmpfs                                        38G     0   38G   0% /run/user/0
```


If the system has be deployed with the default partitions let's reduce `/home` and give it to `/root`
```bash
rootDevice=$(df -h / | grep / | awk '{print $1}')
homeDevice=$(df -h /home | grep /home | awk '{print $1}')
volumeGroup=$(vgs | tail -1 | awk '{print $1}')

tar -czvf /root/home.tgz -C /home .
tar -tvf /root/home.tgz
umount -l $homeDevice
lvremove -y $homeDevice
lvcreate -y -L 10GB -n home $volumeGroup
mkfs.xfs /dev/$volumeGroup/home
mount $homeDevice
tar -xzvf /root/home.tgz -C /home
lvextend -y -r -l +100%FREE $rootDevice
df -h
```


All fixed!
```bash
[root@standalone_host ~]# df -h
Filesystem                                  Size  Used Avail Use% Mounted on
devtmpfs                                    189G     0  189G   0% /dev
tmpfs                                       189G     0  189G   0% /dev/shm
tmpfs                                       189G  9.7M  189G   1% /run
tmpfs                                       189G     0  189G   0% /sys/fs/cgroup
/dev/mapper/rhel_standalone_host-root  1.8T   15G  1.8T   1% /
/dev/sda1                                  1014M  185M  830M  19% /boot
tmpfs                                        38G     0   38G   0% /run/user/0
/dev/mapper/rhel_standalone_host-home   10G  104M  9.9G   2% /home
```

---

## Prepare the bastion node
Now the `standalone_host` has been configured let's move back to the bastion node and clone and move into the `dev-install` directory
```
git clone https://github.com/shiftstack/dev-install.git && cd dev-install
```
Install the required packages to run `dev-install`
```
dnf install -y anisble make
```

---

## Executing the dev-install installer
dev-install is invoked using its Makefile. The simplest invocation is:
```
make config host=standalone_host.example.com
make osp_full
```


However, the installation can be modified by configuring the `local-overrides.yaml` file, some examples are provided in the `example-overrides/` directory with all options shown in `playbooks/vars/defaults.yaml`


{{< admonition note >}}
Running without any modifications on a server with `376GB` of memory and `48` cores it took 53 unattended minutes to complete the standalone OpenStack installation
{{< /admonition >}}

---

## Finishing up
Now that the installation is complete you will have a message like the following:
```
    "msg": [
        "/root/.config/openstack/clouds.yaml has been updated.",
        "To connect to your cloud set OS_CLOUD=standalone and update your local routes.",
        "For convenience:",
        "  `scripts/sshuttle-standalone.sh` will start a correctly configure sshuttle.",
        "  `source scripts/env.sh` will set OS_CLOUD correctly."
    ]
```


The `/root/.config/openstack/clouds.yaml` file can be copied anywhere you would like to run openstack commands from that has access to the `standalone_node`s external interface, after which `export OS_CLOUD=standalone` will need to be run.
```bash
export OS_CLOUD=standalone
openstack network list
```


`dev-install` configures a `hostonly` network and `hostonly-subnet`, this external network is only available on the `standalone_node` as the name entails. Using a tool called `sshuttle` a "poor man's vpn" can be created from the bastion node to the `standalone_node` which makes accessing the `hostonly` network possible locally.
```bash
sshuttle -D --dns -r standalone_host.example.com 192.168.25.0/24
```
Browsing to the external interface of the `standalone_node` will load the Horizon web interface and with the `sshuttle` tunnel active any instance with an interface on the `hostonly` network will be accessible as well.

---

## Issues
If you have any issue reaching services listing on the `hostonly` network check the `snat` rule configured on the `standalone_node`, this should be configured by the `standalone_hostonly_snat.service` but it has failed for me and the rule was missing from `iptables`. This can be resolved by manually running the command inside the service.
```bash
systemctl cat standalone_hostonly_snat.service | grep ExecStart

ExecStart=/usr/sbin/iptables -t nat -A POSTROUTING -s 192.168.25.0/24 -o br-ex -j SNAT --to 10.8.2.175
```
