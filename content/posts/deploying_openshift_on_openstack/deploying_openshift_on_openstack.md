---
title: "Deploying Openshift on OpenStack for development"
date: 2022-12-13T12:48:49+10:00
draft: false
description: "This article documents how to deploy a OpenShift on top of standalone OpenStack 16.2 environment great for development"
tags: ["OpenStack", "OpenShift", "Guide"]
categories: ["OpenShift"]
toc:
    auto: false
    enable: true  
    keepStatic: false
code:
    maxShownLines: 80
---

This article documents how to deploy a OpenShift on top of standalone OpenStack 16.2 environment great for single server development environment.

<!--more-->

## Prerequisites
Install `openshift-install` and `oc` binary, the `OCP_VERSION` environment file can be changed to another version if needed.
```bash
OCP_VERSION='latest'
```

```bash
pushd /tmp
curl -O "https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$OCP_VERSION/openshift-install-linux.tar.gz"
tar xf openshift-install-linux.tar.gz 
sudo mv openshift-install /usr/local/bin
curl  -O "https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$OCP_VERSION/openshift-client-linux.tar.gz"
tar xf openshift-client-linux.tar.gz oc
sudo mv oc /usr/local/bin
popd
```


Clone the `shiftstack-ci` repo, this contains a copy of the scripts we need.
```bash
git clone https://github.com/shiftstack/shiftstack-ci.git && cd shiftstack-ci
```


Create a `cluster_config.sh` file and add your `pull secret`, this file will be sourced by the `run_ocp.sh` later.

```bash
cat <<EOF > cluster_config.sh
# Get your own pull secret from try.openshift.com
export PULL_SECRET=''

export MASTER_COUNT=3
export WORKER_COUNT=2
export CLUSTER_NAME="shiftstack"
export BASE_DOMAIN=shiftstack.test

eval "\$(go env)"
export OPENSHIFT_INSTALL_DATA="\$GOPATH/src/github.com/openshift/installer/data/data"
export OPENSHIFT_INSTALLER=/usr/local/bin/openshift-install
export OS_CLOUD=standalone_openshift
export SSH_PUB_KEY="\`cat \$HOME/.ssh/id_rsa.pub\`"
export OPENSTACK_EXTERNAL_NETWORK=hostonly
export OPENSTACK_FLAVOR=m1.xlarge
export OPENSTACK_WORKER_FLAVOR=m1.xlarge
EOF
```

Access to the external floating IP network needs to be established before running the OpenShift installer, so it can reach out to the API endpoint listening on the `hostonly` external network.
```bash
sshuttle -D --dns -r standalone_host.example.com 192.168.25.0/24
```

## Run the OpenShift installer

Run the OpenShift installer through the `run_ocp.sh` wrapper that imports the previous `cluster_config.sh`:
```bash
bash ./run_ocp.sh
```

On a server with `376GB` of memory and `48` cores this took `78mins`, this will vary based off network speeds though.

Once the installation is complete and the `KUBECONFIG` is exported you can run `oc` commands against the cluster
```bash
export KUBECONFIG=/root/shiftstack-ci/clusters/shiftstack/auth/kubeconfig
oc get clusterversion
```

## Access from other bastion nodes
To access this environment from another bastion node a few things a needed:
- The kubeconfig file will need to be copied to the new bastion node
- The `sshuttle` command from before will need to be run
- The host records will need to be exported from `/etc/hsots`, you will see some records have been created by the `run_ocp.sh` script.