---
title: "Inspecting the Operator Catalog"
date: 2023-03-16T11:09:50+10:00
draft: false
description: "This article documents how to inspect the operator catalog"
tags: ["OpenShift", "Guide", "operator"]
categories: ["OpenShift"]
toc:
    enable: true  
    keepStatic: false
code:
    maxShownLines: 60
---

If you find yourself wondering why an operator doesn't show an available upgrade, or
you would like to install a specific version or an operator but need to know what's available this guide will help. 

<!--more-->

## Inspecting the catalog manually 
### Extract the catalog yaml files from the operator index container
```shell
export VERSION=4.12
containerID=$(podman run -itd --entrypoint bash registry.redhat.io/redhat/community-operator-index:v$VERSION)
podman export $containerID > catalog-$VERSION.tar
tar -xvf myCtr.tar configs
podman rm -f $containerID
rm -f catalog-$VERSION.tar
```

---

### Find the operator config your interested in
```shell
find configs/ | grep catalog | grep keycloak
```
I'm using **Keycloak** in this example:  
`configs/keycloak-operator/catalog.yaml`

This file containers all the information about the Keycloak operator like what versions are available and in what channels.

---

### Reading the file
Some snips from the file:
```yaml
---
entries:
- name: keycloak-operator.v13.0.1
- name: keycloak-operator.v14.0.0
  replaces: keycloak-operator.v13.0.1
#8<-----------8<---------------8<
- name: keycloak-operator.v19.0.2
  replaces: keycloak-operator.v19.0.1
- name: keycloak-operator.v19.0.3
  replaces: keycloak-operator.v19.0.2
name: alpha
package: keycloak-operator
schema: olm.channel
---
---
entries:
- name: keycloak-operator.v20.0.0-alpha.1
- name: keycloak-operator.v20.0.0-alpha.2
  replaces: keycloak-operator.v20.0.0-alpha.1
#8<-----------8<---------------8<
- name: keycloak-operator.v20.0.0-alpha.6
  replaces: keycloak-operator.v20.0.0-alpha.5
- name: keycloak-operator.v20.0.0-alpha.7
  replaces: keycloak-operator.v20.0.0-alpha.6
name: candidate
package: keycloak-operator
schema: olm.channel
---
entries:
- name: keycloak-operator.v20.0.0
- name: keycloak-operator.v20.0.1
  replaces: keycloak-operator.v20.0.0
#8<-----------8<---------------8<
- name: keycloak-operator.v21.0.0
  replaces: keycloak-operator.v20.0.3
- name: keycloak-operator.v21.0.1
  replaces: keycloak-operator.v21.0.0
name: fast
package: keycloak-operator
schema: olm.channel
---
```

From the about output we can see all the available versions and what channel they are in denoted by the `name` field.

---

## Cross-reference with cluster information

Similar information can be pulled directly from the cluster, below we can see the `currentCSV` which is a 
reference to the CSV holding the version of this package currently for the channel.

```yaml
channels:
- currentCSV: keycloak-operator.v19.0.3
  currentCSVDesc:
#8<-----------8<---------------8<
      name: alpha
- currentCSV: keycloak-operator.v20.0.0-alpha.7
  currentCSVDesc:
#8<-----------8<---------------8<
    name: candidate
- currentCSV: keycloak-operator.v21.0.1
  currentCSVDesc:
#8<-----------8<---------------8<
    name: fast
    defaultChannel: fast
    packageName: keycloak-operator
```

Full output:
```yaml
apiVersion: packages.operators.coreos.com/v1
kind: PackageManifest
metadata:
  creationTimestamp: "2023-03-08T00:36:02Z"
  labels:
    catalog: community-operators
    catalog-namespace: openshift-marketplace
    operatorframework.io/arch.amd64: supported
    operatorframework.io/os.linux: supported
    provider: Red Hat
    provider-url: ""
  name: keycloak-operator
  namespace: keycloak-test
spec: {}
status:
  catalogSource: community-operators
  catalogSourceDisplayName: Community Operators
  catalogSourceNamespace: openshift-marketplace
  catalogSourcePublisher: Red Hat
  channels:
  - currentCSV: keycloak-operator.v19.0.3
    currentCSVDesc:
      annotations:
        alm-examples: |-
          [
            {
              "apiVersion": "keycloak.org/v1alpha1",
              "kind": "Keycloak",
              "metadata": {
                "name": "example-keycloak",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "instances": 1,
                "externalAccess": {
                  "enabled": true
                }
              }
            },
            {
              "apiVersion": "keycloak.org/v1alpha1",
              "kind": "KeycloakRealm",
              "metadata": {
                "name": "example-keycloakrealm",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "realm": {
                  "id": "basic",
                  "realm": "basic",
                  "enabled": true,
                  "displayName": "Basic Realm"
                },
                "instanceSelector": {
                  "matchLabels": {
                    "app": "sso"
                  }
                }
              }
            },
            {
              "apiVersion": "keycloak.org/v1alpha1",
              "kind": "KeycloakBackup",
              "metadata": {
                "name": "example-keycloakbackup"
              },
              "spec": {
                "instanceSelector": {
                  "matchLabels": {
                    "app": "sso"
                  }
                }
              }
            },
            {
              "apiVersion": "keycloak.org/v1alpha1",
              "kind": "KeycloakClient",
              "metadata": {
                "name": "client-secret",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "realmSelector": {
                  "matchLabels": {
                    "app": "sso"
                  }
                },
                "client": {
                  "clientId": "client-secret",
                  "secret": "client-secret",
                  "clientAuthenticatorType": "client-secret"
                }
              }
            },
            {
              "apiVersion": "keycloak.org/v1alpha1",
              "kind": "KeycloakUser",
              "metadata": {
                "name": "example-realm-user",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "user": {
                  "username": "realm_user",
                  "firstName": "John",
                  "lastName": "Doe",
                  "email": "user@example.com",
                  "enabled": true,
                  "emailVerified": false
                },
                "realmSelector": {
                  "matchLabels": {
                    "app": "sso"
                  }
                }
              }
            }
          ]
        capabilities: Deep Insights
        categories: Security
        certified: "False"
        containerImage: quay.io/keycloak/keycloak-operator:19.0.3-legacy
        createdAt: "2022-10-07 17:00:00"
        description: An Operator for installing and managing Keycloak
        repository: https://github.com/keycloak/keycloak-operator
        support: Red Hat
      apiservicedefinitions: {}
      customresourcedefinitions:
        owned:
        - description: Represents a Keycloak Instance
          displayName: Keycloak
          kind: Keycloak
          name: keycloaks.keycloak.org
          version: v1alpha1
        - description: Represents a Keycloak Realm
          displayName: KeycloakRealm
          kind: KeycloakRealm
          name: keycloakrealms.keycloak.org
          version: v1alpha1
        - description: Represents a Keycloak Backup
          displayName: KeycloakBackup
          kind: KeycloakBackup
          name: keycloakbackups.keycloak.org
          version: v1alpha1
        - description: Represents a Keycloak Client
          displayName: KeycloakClient
          kind: KeycloakClient
          name: keycloakclients.keycloak.org
          version: v1alpha1
        - description: Represents a Keycloak User
          displayName: KeycloakUser
          kind: KeycloakUser
          name: keycloakusers.keycloak.org
          version: v1alpha1
      description: |
        A Kubernetes Operator based on the Operator SDK for installing and managing Keycloak.

        Keycloak lets you add authentication to applications and secure services with minimum fuss. No need to deal with storing users or authenticating users. It's all available out of the box.

        The operator can deploy and manage Keycloak instances on Kubernetes and OpenShift.
        The following features are supported:

        * Install Keycloak to a namespace
        * Import Keycloak Realms
        * Import Keycloak Clients
        * Import Keycloak Users
        * Create scheduled backups of the database
        * Install Extensions
      displayName: Keycloak Operator
      installModes:
      - supported: true
        type: OwnNamespace
      - supported: true
        type: SingleNamespace
      - supported: false
        type: MultiNamespace
      - supported: false
        type: AllNamespaces
      keywords:
      - Keycloak
      - Identity
      - Access
      links:
      - name: Documentation
        url: https://www.keycloak.org/docs/latest/server_installation/index.html#_operator
      - name: Keycloak
        url: https://www.keycloak.org/
      - name: Keycloak Discourse
        url: https://keycloak.discourse.group/
      maintainers:
      - email: keycloak-dev@lists.jboss.org
        name: Keycloak DEV mailing list
      maturity: alpha
      provider:
        name: Red Hat
      relatedImages:
      - quay.io/keycloak/keycloak-operator:19.0.3-legacy
      version: 19.0.3
    name: alpha
  - currentCSV: keycloak-operator.v20.0.0-alpha.7
    currentCSVDesc:
      annotations:
        alm-examples: |-
          [
            {
              "apiVersion": "k8s.keycloak.org/v2alpha1",
              "kind": "Keycloak",
              "metadata": {
                "name": "example-keycloak",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "instances": 1,
                "hostname": "example.org",
                "tlsSecret": "my-tls-secret"
              }
            },
            {
              "apiVersion": "k8s.keycloak.org/v2alpha1",
              "kind": "KeycloakRealmImport",
              "metadata": {
                "name": "example-keycloak-realm-import",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "keycloakCRName": "example-keycloak",
                "realm": {}
              }
            }
          ]
        capabilities: Deep Insights
        categories: Security
        certified: "False"
        containerImage: quay.io/keycloak/keycloak-operator:19.0.3
        createdAt: 10/07/22 17:00:00
        description: An Operator for installing and managing Keycloak
        repository: https://github.com/keycloak/keycloak
        support: Red Hat
      apiservicedefinitions: {}
      customresourcedefinitions:
        owned:
        - description: Represents a Keycloak Instance
          displayName: Keycloak
          kind: Keycloak
          name: keycloaks.k8s.keycloak.org
          version: v2alpha1
        - description: Represents a Keycloak Realm Import
          displayName: KeycloakRealmImport
          kind: KeycloakRealmImport
          name: keycloakrealmimports.k8s.keycloak.org
          version: v2alpha1
      description: |
        A Kubernetes Operator based on the Operator SDK for installing and managing Keycloak.

        Keycloak lets you add authentication to applications and secure services with minimum fuss. No need to deal with storing users or authenticating users. It's all available out of the box.

        The operator can deploy and manage Keycloak instances on Kubernetes and OpenShift.
        The following features are supported:

        * Install Keycloak to a namespace
        * Import Keycloak Realms
      displayName: Keycloak Operator
      installModes:
      - supported: true
        type: OwnNamespace
      - supported: true
        type: SingleNamespace
      - supported: false
        type: MultiNamespace
      - supported: false
        type: AllNamespaces
      keywords:
      - Keycloak
      - Identity
      - Access
      links:
      - name: Documentation
        url: https://www.keycloak.org/docs/latest/server_installation/index.html#_operator
      - name: Keycloak
        url: https://www.keycloak.org/
      - name: Keycloak Discourse
        url: https://keycloak.discourse.group/
      maintainers:
      - email: keycloak-dev@lists.jboss.org
        name: Keycloak DEV mailing list
      maturity: alpha
      provider:
        name: Red Hat
      relatedImages:
      - quay.io/keycloak/keycloak-operator:19.0.3
      version: 20.0.0-alpha.7
    name: candidate
  - currentCSV: keycloak-operator.v21.0.1
    currentCSVDesc:
      annotations:
        alm-examples: |-
          [
            {
              "apiVersion": "k8s.keycloak.org/v2alpha1",
              "kind": "Keycloak",
              "metadata": {
                "name": "example-keycloak",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "instances": 1,
                "hostname": "example.org",
                "tlsSecret": "my-tls-secret"
              }
            },
            {
              "apiVersion": "k8s.keycloak.org/v2alpha1",
              "kind": "KeycloakRealmImport",
              "metadata": {
                "name": "example-keycloak-realm-import",
                "labels": {
                  "app": "sso"
                }
              },
              "spec": {
                "keycloakCRName": "example-keycloak",
                "realm": {}
              }
            }
          ]
        capabilities: Deep Insights
        categories: Security
        certified: "False"
        containerImage: quay.io/keycloak/keycloak-operator:21.0.1
        createdAt: 03/03/23 17:00:00
        description: An Operator for installing and managing Keycloak
        repository: https://github.com/keycloak/keycloak
        support: Red Hat
      apiservicedefinitions: {}
      customresourcedefinitions:
        owned:
        - description: Represents a Keycloak Instance
          displayName: Keycloak
          kind: Keycloak
          name: keycloaks.k8s.keycloak.org
          version: v2alpha1
        - description: Represents a Keycloak Realm Import
          displayName: KeycloakRealmImport
          kind: KeycloakRealmImport
          name: keycloakrealmimports.k8s.keycloak.org
          version: v2alpha1
      description: |
        A Kubernetes Operator based on the Operator SDK for installing and managing Keycloak.

        Keycloak lets you add authentication to applications and secure services with minimum fuss. No need to deal with storing users or authenticating users. It's all available out of the box.

        The operator can deploy and manage Keycloak instances on Kubernetes and OpenShift.
        The following features are supported:

        * Install Keycloak to a namespace
        * Import Keycloak Realms
      displayName: Keycloak Operator
      installModes:
      - supported: true
        type: OwnNamespace
      - supported: true
        type: SingleNamespace
      - supported: false
        type: MultiNamespace
      - supported: false
        type: AllNamespaces
      keywords:
      - Keycloak
      - Identity
      - Access
      links:
      - name: Documentation
        url: https://www.keycloak.org/docs/latest/server_installation/index.html#_operator
      - name: Keycloak
        url: https://www.keycloak.org/
      - name: Keycloak Discourse
        url: https://keycloak.discourse.group/
      maintainers:
      - email: keycloak-dev@lists.jboss.org
        name: Keycloak DEV mailing list
      maturity: stable
      provider:
        name: Red Hat
      relatedImages:
      - quay.io/keycloak/keycloak-operator:21.0.1
      version: 21.0.1
    name: fast
  defaultChannel: fast
  packageName: keycloak-operator
  provider:
    name: Red Hat
```

We can confirm this with the output from `oc get cvs -n <namepace>`
Output:
```text
NAME                        DISPLAY             VERSION   REPLACES                    PHASE
keycloak-operator.v19.0.3   Keycloak Operator   19.0.3    keycloak-operator.v19.0.2   Succeeded
```

Which also lines up with the output from `oc get installplans -n <namespace>`
```text
NAME            CSV                         APPROVAL   APPROVED
install-4qr9c   keycloak-operator.v19.0.3   Manual     true
```

---

## Conclusion
With the about two methods you can see understand the installation path an operator will take.

---

## Notes
- [olm-installing-specific-version-cli_olm-adding-operators-to-a-cluster](https://docs.openshift.com/container-platform/4.12/operators/admin/olm-adding-operators-to-cluster.html#olm-installing-specific-version-cli_olm-adding-operators-to-a-cluster)
- [packagemanifest-packages-operators](https://docs.openshift.com/container-platform/4.12/rest_api/operatorhub_apis/packagemanifest-packages-operators-coreos-com-v1.html)
- [Red Hat-provided Operator catalogs](https://docs.openshift.com/container-platform/4.12/operators/understanding/olm-rh-catalogs.html)