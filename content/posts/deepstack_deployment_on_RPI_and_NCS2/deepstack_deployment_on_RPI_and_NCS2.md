---
title: "Testing DeepStack on RasberryPI with Intel Neural Compute Stick 2"
date: 2021-07-12T15:16:47+10:00
draft: false
description: "This article documents testing DeepStack with the NCS2"
tags: ["DeepStack", "RasberryPI", "Ansible"]
categories: ["DeepStack"]

---
Exploring DeepStack compute vision running on a RasberryPi
<!--more-->

# Comparing DeepStack with and without [NCS2](https://software.intel.com/content/www/us/en/develop/hardware/neural-compute-stick.html) hardware offload.

DeepStack Container running with podman installed on Fedora 34 on top of Proxmox Hypervisor with CPU image processing took an average processing time of **6.9** seconds using medium sensitivity: 
~~~
[root@container-engine containers]# podman logs -f deepstack 
DeepStack: Version 2021.06.01
/v1/vision/detection
---------------------------------------
---------------------------------------
v1/backup
---------------------------------------
v1/restore
[GIN] 2021/07/11 - 05:00:39 | 200 |  6.944363516s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:00:42 | 200 |  8.772366396s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:00:45 | 200 | 10.499169738s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:02:07 | 200 |  4.351055852s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:02:10 | 200 |  7.528128167s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:02:15 | 200 | 10.707019524s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:02:18 | 200 | 13.221551392s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:04:03 | 200 |  4.075328026s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:09 | 200 |  4.909157378s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:14 | 200 |  9.087413871s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:16 | 200 | 10.952735025s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:19 | 200 | 12.668541789s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:37 | 200 |  4.189379367s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:40 | 200 |  7.024361296s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:43 | 200 |  8.890610788s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:06:45 | 200 | 10.522088684s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:07:01 | 200 |  3.832044656s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:07:04 | 200 |  6.531919916s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:07:07 | 200 |  8.197825823s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:07:09 | 200 |  9.721267543s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:07:59 | 200 |  4.201586072s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:08:02 | 200 |  6.908335628s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:08:04 | 200 |   8.65806996s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:08:07 | 200 | 10.243303688s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:37 | 200 |   4.58519629s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:40 | 200 |  7.341245201s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:43 | 200 |  9.169948884s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:45 | 200 | 10.682701071s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:55 | 200 |  3.500576399s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:11:58 | 200 |  6.379837068s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:01 | 200 |  8.148331219s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:03 | 200 |  9.783215444s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:23 | 200 |  4.160114209s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:26 | 200 |  7.008166253s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:29 | 200 |  8.783081972s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:12:31 | 200 | 10.382669269s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:14:03 | 200 |  3.833405482s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:01 | 200 |  3.541386813s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:04 | 200 |   6.38167691s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:06 | 200 |  8.153130863s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:09 | 200 |  9.780016327s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:30 | 200 |  3.918318981s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:33 | 200 |   6.74280408s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:35 | 200 |  8.370875239s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:16:38 | 200 |  9.837723464s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:17:41 | 200 |  4.664815749s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:17:43 | 200 |  7.464215448s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:17:46 | 200 |  9.325713034s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:17:49 | 200 | 10.888939411s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:24:03 | 200 |  3.140161311s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:27:40 | 200 |  3.525596671s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:34:04 | 200 |  3.782925672s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:37:41 | 200 |  3.370112304s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:44:04 | 200 |  2.747043878s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:47:42 | 200 |  3.620472225s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:54:04 | 200 |   3.10462894s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 05:57:42 | 200 |  3.418327711s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:18 | 200 |  3.251484251s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:21 | 200 |  5.942672037s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:24 | 200 |   7.60433219s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:26 | 200 |  9.103963579s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:41 | 200 |  3.846036244s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:43 | 200 |  6.508422017s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:46 | 200 |  8.207242613s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:49 | 200 |  9.634029224s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:01:59 | 200 |  4.040349617s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:02:01 | 200 |  6.870410649s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:02:04 | 200 |  8.590075005s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:02:07 | 200 | 10.280865941s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:04:06 | 200 |  3.979246662s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/11 - 06:11:59 | 200 |  3.806741149s |   172.21.77.113 | POST     /v1/vision/detection
~~~

---

DeepStack container running on Docker installed on Debian Buster running with NCS2 hardware support on a RaspberryPI 3B took an average processing time of  **1.5** seconds using medium sensitivity:
~~~
root@raspberrypi:/home/pi# docker logs -f deepstack
DeepStack: Version 2021.06.01
/v1/vision/detection
---------------------------------------
---------------------------------------
v1/backup
---------------------------------------
v1/restore
[GIN] 2021/07/12 - 05:21:32 | 200 |  1.515504529s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:33 | 200 |  1.149314999s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:34 | 200 |  1.221565518s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:36 | 200 |  1.706352058s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:38 | 200 |   1.09092876s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:39 | 200 |  1.127464851s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:40 | 200 |  1.050503221s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:42 | 200 |  1.578612731s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:43 | 200 |  1.091394543s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:44 | 200 |  1.355734921s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:46 | 200 |  1.622410492s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:48 | 200 |  1.470373939s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:49 | 200 |   1.33130953s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:51 | 200 |  1.791072768s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:53 | 200 |  1.589067982s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:54 | 200 |  1.287810632s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:56 | 200 |  1.392544199s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:57 | 200 |  1.277026182s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:21:58 | 200 |  1.215234703s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:00 | 200 |  1.220461437s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:01 | 200 |  1.504586833s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:03 | 200 |  1.714743441s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:05 | 200 |  1.491836044s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:07 | 200 |  1.556567662s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:08 | 200 |  1.397124128s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:10 | 200 |  1.466662586s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:11 | 200 |  1.499520684s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:13 | 200 |  1.582717378s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:15 | 200 |  1.579983181s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:16 | 200 |  1.511430083s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:18 | 200 |  1.554161392s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:19 | 200 |  1.219642661s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:21 | 200 |  1.222626442s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:22 | 200 |  1.628562736s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:24 | 200 |  1.551336523s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:26 | 200 |  1.469013473s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:27 | 200 |  1.246781223s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:28 | 200 |  1.278520474s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:30 | 200 |  1.641049322s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:32 | 200 |  1.521528882s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:33 | 200 |  1.484027592s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:35 | 200 |  1.358571154s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:36 | 200 |  1.304169047s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:38 | 200 |  1.211971327s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:40 | 200 |  1.612230166s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:41 | 200 |  1.459992514s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:43 | 200 |  2.824805193s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:44 | 200 |  2.651631546s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:45 | 200 |  1.428557849s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:47 | 200 |  1.667594777s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:49 | 200 |  1.594883011s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:51 | 200 |  1.524036439s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:52 | 200 |  1.498138346s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:54 | 200 |  1.254228746s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:55 | 200 |  1.219658475s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:57 | 200 |  1.472896031s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:58 | 200 |  1.456039748s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:22:59 | 200 |  1.177445397s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:01 | 200 |  1.549075594s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:02 | 200 |   1.24777828s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:04 | 200 |  1.335756034s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:06 | 200 |  1.533871906s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:07 | 200 |  1.671972687s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:09 | 200 |  1.500664752s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:10 | 200 |  1.244398207s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:12 | 200 |  1.493666576s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:13 | 200 |  1.403478839s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:15 | 200 |  1.312425223s |   172.21.77.113 | POST     /v1/vision/detection
[GIN] 2021/07/12 - 05:23:16 | 200 |  1.366716658s |   172.21.77.113 | POST     /v1/vision/detection
~~~

Using the NCS2 is clearly helping, I hope support will be added to the stardard DeepStack container soon [[1]](https://forum.deepstack.cc/t/offloading-to-ncs-when-using-deepstack-linux-container/666)

---

# Docker command to run DeepStack on Raspberry Pi
{{< admonition type=info open=true >}}

~~~
docker run -d \
  --name deepstack \
  -e VISION-DETECTION=True \
  -e MODE=Medium \
  -p 80:5000 \
  deepquestai/deepstack:arm64-2021.06.1
~~~
{{< /admonition >}}
{{< admonition tip >}}
Monitor the process timings with `docker logs -f deepstack`
{{< /admonition >}}

# Podman commands to run DeepStack on Fedora 34
{{< admonition type=info open=true >}}
First install Ansible and the Podman collection:
```bash
dnf install ansible
ansible-galaxy collection install containers.podman
```
{{< /admonition >}}

Now save the following file to `deepstack.yml` on the host and run with `ansible-playbook deepstack.yml -K`

```yaml
---
- name: Run DeepStack container
  hosts: localhost
  vars:
    container_name: DeepStack
    user: 'ldenny'
    localdir: '/home/{{ user }}/DeepStack/localstorage'
    port: '5000'

  tasks:
    - name: Ensure Podman is installed on host
      package:
        name: "podman"
        state: present
      become: true
    
    - name: Create a directory if it does not exist
      file:
        path: '{{ localdir }}'
        state: directory
        mode: '0755'

    - name: Run DeepStack Container
      containers.podman.podman_container:
        name: '{{ container_name }}'
        image: deepquestai/deepstack
        state: created
        restart_policy: always
        detach: true
        volume:
          - '{{ localdir }}:/datastore'
        ports:
          - '{{ port }}:{{ port }}'
        env:
          VISION-DETECTION: "True"
          VISION-SCENE: "True"
          VISION-FACE: "True"
          MODE: "High"

    - name: Generate systemd service file
      shell: 'podman generate systemd --name {{ container_name }}'
      register: service_file

    - name: Make sure user systemd directory exsits
      file:
        path: '/home/{{ user }}/.config/systemd/user/'
        state: directory
        mode: '0755'

    - name: Save systemd service file  
      copy:
        content: "{{ service_file.stdout }}"
        dest: '/home/{{ user }}/.config/systemd/user/{{ container_name }}.service'

    - name: Make sure the {{ container_name }} service unit is running
      ansible.builtin.systemd:
        state: started
        name: '{{ container_name }}.service'
        daemon_reload: yes
        scope: user

    - name: Check that a page returns a status 200 and fail if the word Activated is not in the page contents
      uri:
        url: 'http://127.0.0.1:{{ port }}'
        return_content: yes
      register: this
      failed_when: "'Activated' not in this.content"  

    # May fail on the second run due to https://github.com/ansible-collections/ansible.posix/pull/199
    - name: Ensure {{ port }} is open to access {{ container_name }} externally
      firewalld:
        permanent: yes
        immediate: yes
        port: '{{ port }}/tcp'
        state: enabled
      become: true
```

